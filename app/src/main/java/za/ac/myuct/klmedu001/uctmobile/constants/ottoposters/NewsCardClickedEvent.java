package za.ac.myuct.klmedu001.uctmobile.constants.ottoposters;

/**
 * Created by eduardokolomajr on 2014/07/28.
 */
public class NewsCardClickedEvent {
    public final String title;

    public NewsCardClickedEvent(String title) {
        this.title = title;
    }
}
