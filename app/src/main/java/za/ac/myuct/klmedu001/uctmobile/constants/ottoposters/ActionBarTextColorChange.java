package za.ac.myuct.klmedu001.uctmobile.constants.ottoposters;

/**
 * Created by eduardokolomajr on 2014/11/07.
 */
public class ActionBarTextColorChange {
    public final int color;

    public ActionBarTextColorChange(int color) {
        this.color = color;
    }
}
