package za.ac.myuct.klmedu001.uctmobile.processes;

import android.view.View;

/**
 * Created by eduardokolomajr on 2014/10/26.
 */
public class AnimatorHeightHolder {
    public final View view;
    public final int endHeight;

    public AnimatorHeightHolder(View view, int endHeight) {
        this.view = view;
        this.endHeight = endHeight;
    }
}
