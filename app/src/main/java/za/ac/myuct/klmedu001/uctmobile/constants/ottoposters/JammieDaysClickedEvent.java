package za.ac.myuct.klmedu001.uctmobile.constants.ottoposters;

/**
 * Created by eduardokolomajr on 2014/09/23.
 */
public class JammieDaysClickedEvent {
    public final char dayChar;

    public JammieDaysClickedEvent(char dayChar) {
        this.dayChar = dayChar;
    }
}
