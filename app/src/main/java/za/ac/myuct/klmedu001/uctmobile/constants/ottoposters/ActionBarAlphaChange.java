package za.ac.myuct.klmedu001.uctmobile.constants.ottoposters;

/**
 * Created by eduardokolomajr on 2014/11/06.
 */
public class ActionBarAlphaChange {
    public final int alphaVal;

    public ActionBarAlphaChange(int alphaVal) {
        this.alphaVal = alphaVal;
    }
}
